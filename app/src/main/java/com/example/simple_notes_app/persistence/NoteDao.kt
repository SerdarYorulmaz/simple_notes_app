package com.example.simple_notes_app.persistence

import androidx.room.*

interface NoteDao {

    /**
    OnConflictStrategy.REPLACE :Çeşitli Daoyöntemler için çatışma yönetimi stratejileri saglar .

         onConflict
         Bir çakışma olursa  ne yapılmalı

     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(note:Note)

    @Update
    fun update(note:Note)

    @Delete
    fun delete(note:Note)

    @Query("Select * from tbl_note") //tum tbl_note sutunlarini getir
    fun getAllNotes():List<Note>  /** tum notlari getir */




}