package com.example.simple_notes_app.persistence

import androidx.room.Entity

@Entity(tableName = "tbl_note")
class Note(
    var id:Int,
    var title:String?,
    var description:String?,
    var tag:String?
){}